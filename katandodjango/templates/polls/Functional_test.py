__author__ = 'wilson'

from unittest import TestCase
from selenium import webdriver
from selenium.webdriver.common.by import By

class FunctionalTest (TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_title(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('Busco Ayuda', self.browser.title)

    def test_registro(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('id_register')
        link.click()

        nombre = self.browser.find_element_by_id('id_nombres')
        nombre.send_keys('Juan Daniel')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.send_keys('Arevalo7')

        experiencia = self.browser.find_element_by_id('id_experiencia')
        experiencia.send_keys('5')

        self.browser.find_element_by_xpath("//select[@id='id_servicio']/option[text()='Desarrollador Web']").click()
        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.send_keys('3173024578')

        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('jd.patino22@uniandes.edu.co')

        imagen = self.browser.find_element_by_id('id_foto')
        imagen.send_keys('C:\Users\LeidyJohanna\Pictures\ela.jpg')

        clave = self.browser.find_element_by_id('id_password')
        clave.send_keys('clave123')

        clave = self.browser.find_element_by_id('id_password_confirm')
        clave.send_keys('clave123')

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()
        self.browser.implicitly_wait(10)
        span=self.browser.find_element_by_name("Juan Daniel Arevalo7")

        self.assertIn('Juan Daniel Arevalo7', span.text)

    def test_verDetalle(self):
        self.browser.get('http://localhost:8000')
        span=self.browser.find_element_by_id("Juan Daniel Arevalo7")
        span.click()

        h3=self.browser.find_element_by_name("Juan Daniel Arevalo7")

        self.assertIn('Juan Daniel Arevalo7', h3.text)

    def test_login(self):
        self.browser.get('http://localhost:8000')
        span=self.browser.find_element_by_name("login")
        span.click()

        usuario = self.browser.find_element_by_name('usrname')
        usuario.send_keys('admin')

        password = self.browser.find_element_by_name('psw')
        password.send_keys('Warcraft5')

        span2=self.browser.find_element_by_name("entrar")
        span2.click()

        self.browser.implicitly_wait(10)

        h3=self.browser.find_element_by_name("autenticado")
