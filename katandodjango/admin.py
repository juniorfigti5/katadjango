from django.contrib import admin

# Register your models here.
from katandodjango.models import Servicio, Independiente

admin.site.register(Servicio)
admin.site.register(Independiente)